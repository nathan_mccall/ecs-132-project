# Problem A
Use `getDataA()` after importing `data.R` to fetch the data from Matloff's
site needed to work on Problem A

# Problem B
**Note:** you will need to type `install.packages(c("XML", "tm", "ggplot2"))` in the interactive R shell and select a mirror to get the libraries we need

WIP, right now we have the following functions:

- `getLinks(url, regex)` takes a url and grabs all visible links, then filters the results according to the regex
- `getTex(base.url)` uses getLinks to fetch a list of all tex files reachable in course/*Exams/
- `parseFile(url)` will return a tm Corpus object with the contents of the document at url. Currently converts to lowercase, removes punctuation and stopwords (see `help(stopwords)` with tm installed), and strips whitespace. Pass it urls you get from `getTex()`. If you're in the shell you can view the results with `strwrap(doc)` where `doc` is something assigned the result of a call to `parseFile()`
- `getFiles(urls)` will fetch the files at urls and return the contents as a cleaned Corpus. This will convert to lowercase, remove the punctuation, stopwords (the, is, and, but...), and whitespace. The corpus should be converted to a DocumentTermMatrix with `dtm <- DocumentTermMatrix(corpus)`

The final matrix I am working on will have the following structure:

| doc |   [1-25]   | is.50 | is.132 | is.145 | is.152a | is.154a | is.154b | is.158 | is.256 |
| --- | ---------- | ----- | ------ | ------ | ------- | ------- | ------- | ------ | ------ |
| 1-n | word count |  0/1  |   0/1  |   0/1  |   0/1   |   0/1   |   0/1   |   0/1  |   0/1  |

The columns `is.[course_num]` indicate whether the word counts in the first 20 columns belong to a document from that course. Calls to `glm()` should then be of the form
```
links <- getTex("http://heather.cs.ucdavis.edu/~matloff/")
files <- getFiles(links)
dtm <- DocumentTermMatrix(files)
# TODO: need some logic here to remove most frequent and least frequent words
m <- as.matrix(dtm)
# TODO: need to add code here to append the course membership int columns
model <- glm(m[, 'is.<num>'] ~ m[, 1:25])
``` 
