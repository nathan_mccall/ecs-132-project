The approach for finding these values is fairly straightforward. In order to determine the mean quiz average for each course we need to split the data based on the course number and then find the sample mean for each course. These sample means -- and their corresponding confidence intervals -- can then be used to estimate the true population quiz average for each course. In order to develop some intuition about what the means should be we can look at a bar frequency plot of the grades for each course. Figure \ref{fig:GradeBar} shows the relative frequency of grade averages from $0-4.5$ in $0.5$ width increments.

	\begin{figure}[H]
		\centering
		\begin{tabular}{cc}
			\includegraphics[width=2.75in]{all-grades}    & \includegraphics[width=2.75in]{132-grades} \\
			(a) Grades for all courses combined & (b) Grades for ECS 132 \\[8pt]
			\includegraphics[width=2.75in]{145-grades}    & \includegraphics[width=2.75in]{158-grades} \\
			(c) Grades for ECS 145              & (d) Grades for ECS 158 \\[8pt]
		\end{tabular}
		\caption{Frequency plots of the grade averages in each course}
		\label{fig:GradeBar}
	\end{figure}

We then can calculate the mean using the equation

\begin{equation}
	\label{xbardef}
	\overline{X} = \frac{X_1 + X_2 + X_3 + ... + X_n}{n}
\end{equation}

Where the numerator represents the sum of all the grade averages in a given course and the denominator is the size of the course sample. The sample means alone, however, are not enough to estimate the population mean. In order to do that we also need to find the standard error $s$, which can be found by taking the square root of the sample variance. The sample variance then is found with

\begin{align}
	\label{sigma2def}
	Var(\overline{X}) &= E(\overline{X}^2) - (E\overline{X})^2 \\
	s &= \sqrt{Var(\overline{X})}
\end{align}

We know from the text that standard error is the square root of the sample variance, and we are also given that the confidence interval can be written as

\begin{equation}
	(\overline{X} - 1.96 \frac{s}{\sqrt{n}}, \overline{X} + 1.96 \frac{s}{\sqrt{n}})
\end{equation}

Where s is our standard error found via the method above. These values are all calculated using the function \texttt{problemA1()} in \texttt{data.R} which returns a data frame containing the following

\begin{table}[h]
\centering
\begin{tabular}{|r|l|l|l|l|l|}
	\hline
	Course  & Mean  & Variance & Std. Err. & CI Low & CI High \\
	\hline
	\hline
	ECS 132 & 2.650 & 0.6967   & 0.8347    & 2.542  & 2.759 \\
	ECS 145 & 3.282 & 0.4283   & 0.6545    & 3.161  & 3.403 \\
	ECS 145 & 3.207 & 0.4661   & 0.6827    & 3.099  & 3.314 \\
	\hline
\end{tabular}
\end{table}

What this then means is we are 95\% likely to find the population quiz grade averages within the following intervals:

\begin{quote}
ECS 132: (2.542, 2.759) \\
ECS 145: (3.161, 3.403) \\
ECS 158: (3.099, 3.314)
\end{quote}

Which roughly agrees with the distributions shown in Figure \ref{fig:GradeBar} above. We see that ECS 132 has the lowest average, followed by ECS 158 and then ECS 145. Therefore we can say that the above data is a fair estimate of the true population quiz average for each course.
